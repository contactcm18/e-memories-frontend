import { NextRouter } from "next/router";

export interface INavLink {
  text: string;
  href: string;
  auth?: boolean | undefined;
}

export const navLinks: INavLink[] = [
  { text: "Login", href: "/login", auth: false },
  { text: "Memories", href: "/memories", auth: true },
  { text: "Profile", href: "/profile", auth: true },
];

export const useLink = (link: INavLink, isLoggedIn: boolean) =>
  link.auth == null || (isLoggedIn && link.auth) || (!isLoggedIn && !link.auth);

export const isActiveLink = (href: string, router: NextRouter, styles) =>
  router.pathname === href ? styles.linkActive : "";
