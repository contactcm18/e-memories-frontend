import { IncomingMessage, ServerResponse } from "http";

export interface IQuote {
  quote: {
    text: string;
    author: string;
  };
}

export const getQuote = (): Promise<IQuote> =>
  new Promise<IQuote>((resolve, reject) => {
    try {
      const data = fetch(
        `${process.env.NEXT_PUBLIC_BACKEND_HOST}/user/getQuote`
      ).then((r) => r.json());
      resolve(data);
    } catch (error) {
      resolve({
        quote: { text: "Failed to fetch quote.", author: "Apologies" },
      });
    }
  });

/**
 * Always resolves and returns a boolean.
 */
export const isAuthenticated = async (
  request: IncomingMessage,
  response: ServerResponse = null
): Promise<{ authenticated: boolean; email: string }> =>
  new Promise(async (resolve, reject) => {
    try {
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/isAuthenticated`,
        {
          method: "POST",
          mode: "cors",
          credentials: "include",
          headers: {
            Cookie: request.headers.cookie,
          },
        }
      );

      if (res.status !== 200) throw Error("Response status is not 200.");
      else {
        if (response) {
          const data = await res.json();
          relayResCookiesToClient(res, response);
          resolve({ authenticated: true, email: data.email });
        }
      }
    } catch (error) {
      resolve({ authenticated: false, email: "" });
    }
  });

/**
 * When a fetch request is made and a response is recieved,
 * relay the response cookies back to the client.
 * Server makes request -> Receives cookies from request -> Relays them to client
 * @param response The response from fetch.
 * @param res The res on ssr that is sent back to the client.
 */
const relayResCookiesToClient = (response: Response, res: ServerResponse) => {
  const cookies = response.headers.get("set-cookie");
  if (cookies) {
    const regexMatch = /(accessToken.*),\s(refreshToken.*)/g.exec(cookies);
    const cookiesToArr = [regexMatch[1], regexMatch[2]];
    res.setHeader("Set-Cookie", cookiesToArr);
  }
};
