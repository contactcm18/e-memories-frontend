import { createWrapper } from "next-redux-wrapper";
import { createStore } from "redux";
import reducers from "store/reducers";

const makeStore = (context) => createStore(reducers);

export const wrapper = createWrapper(makeStore);
