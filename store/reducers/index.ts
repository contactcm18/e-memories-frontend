import { combineReducers } from "redux";

import { authenticatedReducer } from "../reducers/authenticated";

export default combineReducers({
  authenticated: authenticatedReducer,
});
