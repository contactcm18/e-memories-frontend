import { HYDRATE } from "next-redux-wrapper";
import * as types from "../types";

export const authenticatedReducer = (
  state = { authenticated: false },
  action
) => {
  switch (action.type) {
    case HYDRATE:
      console.log("HYDRATE");
      console.log(state);
      console.log(action.payload.authenticated);
      return {
        ...state,
        ...action.payload.authenticated,
      };
    case types.SET_AUTHENTICATION:
      return action.payload;
    default:
      return state;
  }
};
