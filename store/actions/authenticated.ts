import * as types from "../types";

export const setAuthenticated = (value) => ({
  type: types.SET_AUTHENTICATION,
  payload: { authenticated: value },
});
