import Link from "next/link";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { navLinks, useLink, isActiveLink } from "lib/nav";

import styles from "./styles.module.scss";

function SideBar({ isLoggedIn, toggleSideNav, onToggleSideNav }) {
  const router = useRouter();

  if (isLoggedIn == null) return <div></div>;

  return (
    <nav className={`row ${styles.nav} ${toggleSideNav ? styles.toggle : ""}`}>
      <div className="col-2"></div>
      <ul className="col-20">
        {navLinks.map((link) =>
          useLink(link, isLoggedIn) ? (
            <li
              key={link.href}
              className={isActiveLink(link.href, router, styles)}
            >
              <Link href={link.href}>
                <a onClick={onToggleSideNav}>{link.text}</a>
              </Link>
            </li>
          ) : (
            ""
          )
        )}
      </ul>
      <div className="col-2"></div>
    </nav>
  );
}

SideBar.propTypes = {
  isLoggedIn: PropTypes.any,
  toggleSideNav: PropTypes.bool.isRequired,
  onToggleSideNav: PropTypes.func.isRequired,
};

export default SideBar;
