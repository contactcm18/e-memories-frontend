import { FormEvent, useState } from "react";
import PropTypes from "prop-types";
import styles from "./styles.module.scss";

function CreateMemory({ onCreateMemory }) {
  const [memoryText, editMemoryText] = useState("");

  const createMemory = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!memoryText) return;
    onCreateMemory(memoryText);
    editMemoryText("");
  };

  return (
    <form onSubmit={createMemory} className={styles.createMemoryForm}>
      <input
        type="text"
        name="your memory"
        value={memoryText}
        onChange={(e) => editMemoryText(e.target.value)}
      />
      <button type="submit">Create</button>
    </form>
  );
}

CreateMemory.propTypes = {
  onCreateMemory: PropTypes.func.isRequired,
};

export default CreateMemory;
