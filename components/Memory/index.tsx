import PropTypes from "prop-types";
import styles from "./styles.module.scss";

function Memory({ memory, onDeleteMemory }) {
  const deleteMemory = () => {
    // Todo send API request
    onDeleteMemory(memory.id);
  };

  return (
    <li className={styles.memory}>
      {memory.text}
      <button onClick={deleteMemory}>Delete</button>
    </li>
  );
}

Memory.propTypes = {
  memory: PropTypes.object.isRequired,
  onDeleteMemory: PropTypes.func.isRequired,
};

export default Memory;
