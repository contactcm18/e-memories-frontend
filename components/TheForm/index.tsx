import { FormEvent } from "react";
import PropTypes from "prop-types";
import styles from "./styles.module.scss";

function TheForm({
  title,
  image,
  onSubmit,
  isLoading,
  mainFormElements,
  errorElements,
  successElements,
  endElements,
}) {
  const onSubmitForm = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onSubmit(event);
  };

  return (
    <main className={`row contain ${styles.reusableFormPage}`}>
      <div
        className={`col-24 col-md-6 ${styles.image}`}
        style={{ backgroundImage: `url(${image})` }}
      >
        <div></div>
      </div>
      <div className="d-none d-md-block col-md-1"></div>
      <div className={`col-md-8 ${styles.formWrapper}`}>
        <form onSubmit={onSubmitForm}>
          <h1>{title}</h1>
          {mainFormElements}
          <ul className={styles.errorElements}>{errorElements}</ul>
          <ul className={styles.successElements}>{successElements}</ul>
          <button type="submit" className={isLoading ? "loading" : ""}>
            Submit
          </button>
          {endElements}
        </form>
      </div>
    </main>
  );
}

TheForm.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  mainFormElements: PropTypes.element.isRequired,
  errorElements: PropTypes.element,
  successElements: PropTypes.element,
  endElements: PropTypes.element.isRequired,
};

export default TheForm;
