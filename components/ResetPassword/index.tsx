import TheForm from "components/TheForm";
import Link from "next/link";
import React, { useState } from "react";
import PropTypes from "prop-types";

function ResetPassword({
  onResetPassword,
  mainFormElements,
  successElements,
  errorElements,
}) {
  const [isLoading, setIsLoading] = useState(false);

  const resetPassword = async () => {
    if (isLoading) return;
    setIsLoading(true);
    await onResetPassword();
    setIsLoading(false);
  };

  const endElements = (
    <React.Fragment>
      <Link href="/login">
        <a>Remember your password?</a>
      </Link>
    </React.Fragment>
  );

  return (
    <TheForm
      title="Reset Password"
      image="/images/forgot_password/security.jpg"
      onSubmit={resetPassword}
      isLoading={isLoading}
      mainFormElements={mainFormElements}
      errorElements={errorElements}
      successElements={successElements}
      endElements={endElements}
    />
  );
}

ResetPassword.propTypes = {
  onResetPassword: PropTypes.func.isRequired,
  mainFormElements: PropTypes.element.isRequired,
  successElements: PropTypes.element.isRequired,
  errorElements: PropTypes.element.isRequired,
};

export default ResetPassword;
