import styles from "./styles.module.scss";

function FullLoad() {
  return (
    <section aria-label="Loading..." className={styles.loadPage}>
      <div className={styles.loader}></div>
    </section>
  );
}

export default FullLoad;
