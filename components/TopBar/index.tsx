import Link from "next/link";
import { useRouter } from "next/router";

import PropTypes from "prop-types";
import { navLinks, useLink, isActiveLink } from "lib/nav";

import styles from "./styles.module.scss";

function TopBar({ isLoggedIn, toggleSideNav, onToggleSideNav }) {
  const router = useRouter();
  if (isLoggedIn == null) return <div></div>;

  return (
    <nav className={`row ${styles.nav}`}>
      <div className="col-2"></div>
      <ul className="col-20">
        <li className={isActiveLink("/", router, styles)}>
          <Link href="/">
            <a onClick={() => onToggleSideNav(false)}>Home</a>
          </Link>
        </li>
        {navLinks.map((link) =>
          useLink(link, isLoggedIn) ? (
            <li
              key={link.href}
              className={isActiveLink(link.href, router, styles)}
            >
              <Link href={link.href}>
                <a>{link.text}</a>
              </Link>
            </li>
          ) : (
            ""
          )
        )}
        <li className={`nav-button ${toggleSideNav ? "toggle" : ""}`}>
          <button
            aria-label="Toggle mobile navigation"
            onClick={onToggleSideNav}
          >
            <div className="circle"></div>
            <div className="exit">
              <div></div>
              <div></div>
            </div>
          </button>
        </li>
      </ul>
      <div className="col-2"></div>
    </nav>
  );
}

TopBar.propTypes = {
  isLoggedIn: PropTypes.any,
  toggleSideNav: PropTypes.bool.isRequired,
  onToggleSideNav: PropTypes.func.isRequired,
};

export default TopBar;
