import PropTypes from "prop-types";
import styles from "./styles.module.scss";

function Quote({ quote }) {
  return (
    <figure className={styles.quoteFigure}>
      <blockquote>"{quote.text}"</blockquote>
      <figcaption>- {quote.author}</figcaption>
    </figure>
  );
}

Quote.propTypes = {
  quote: PropTypes.object.isRequired,
};

export default Quote;
