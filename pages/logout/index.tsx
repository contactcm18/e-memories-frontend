import React from "react";
import { useEffect } from "react";
import { useRouter } from "next/router";
import Head from "next/head";

import FullLoad from "components/FullLoad";

function Logout() {
  const router = useRouter();

  useEffect(() => {
    try {
      fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/logout`, {
        method: "POST",
        credentials: "include",
      }).then(() => {
        router.push("/login");
      });
    } catch (error) {}
  });

  return (
    <React.Fragment>
      <Head>
        <title>Logout - E-Memories</title>
      </Head>
      <FullLoad />
    </React.Fragment>
  );
}

export default Logout;
