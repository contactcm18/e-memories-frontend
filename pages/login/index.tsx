import React, { FormEvent, useState } from "react";
import Link from "next/link";
import Router from "next/router";
import { GetServerSideProps } from "next";

import { isAuthenticated } from "lib/fetch";
import TheForm from "components/TheForm";

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const { authenticated } = await isAuthenticated(req, res);
  return {
    ...(authenticated && {
      redirect: {
        destination: "/profile",
        statusCode: 302,
      },
    }),
    props: {},
  };
};

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const initialErrorElementsState = <React.Fragment></React.Fragment>;
  const [errorElements, setErrorElements] = useState(initialErrorElementsState);

  const onSubmitLogin = async (event: FormEvent<HTMLFormElement>) => {
    if (isLoading) return;
    setIsLoading(true);

    try {
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/login`,
        {
          method: "POST",
          credentials: "include",
          mode: "cors",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email,
            password,
          }),
        }
      );

      if (res.status !== 200) {
        const data = await res.json();
        setErrorElements(
          <React.Fragment>
            {data.map((error) => (
              <li key={error}>{error}</li>
            ))}
          </React.Fragment>
        );
        throw Error();
      } else {
        Router.push("/profile");
      }
    } catch (error) {
      setIsLoading(false);
    }
  };

  const mainFormElements = (
    <React.Fragment>
      <div className="input-wrapper">
        <img src="/images/icons/mail.svg" alt="email" />
        <input
          type="text"
          name="email"
          placeholder="Your email..."
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div className="input-wrapper">
        <img src="/images/icons/password.svg" alt="password" />
        <input
          type="password"
          name="password"
          placeholder="Your password..."
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
    </React.Fragment>
  );

  const endElements = (
    <React.Fragment>
      <Link href="/resetPassword">
        <a>Forgot password?</a>
      </Link>
      <Link href="/register">
        <a>Don't have an account?</a>
      </Link>
    </React.Fragment>
  );

  return (
    <TheForm
      title="Login"
      onSubmit={onSubmitLogin}
      mainFormElements={mainFormElements}
      image="/images/login/memory.jpg"
      endElements={endElements}
      isLoading={isLoading}
      errorElements={errorElements}
    />
  );
}

export default Login;
