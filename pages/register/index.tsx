import React, { FormEvent, useState } from "react";
import Head from "next/head";
import Link from "next/link";
import Router from "next/router";
import { GetServerSideProps } from "next";

import { isAuthenticated } from "lib/fetch";
import TheForm from "components/TheForm";

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const { authenticated } = await isAuthenticated(req, res);
  return {
    ...(authenticated && {
      redirect: {
        destination: "/profile",
        statusCode: 302,
      },
    }),
    props: {},
  };
};

function Register() {
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const initialErrorElementsState = <React.Fragment></React.Fragment>;
  const [errorElements, setErrorElements] = useState(initialErrorElementsState);

  const onSubmitRegister = async (event: FormEvent<HTMLFormElement>) => {
    // Register
    if (isLoading) return;
    setIsLoading(true);

    try {
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/register`,
        {
          method: "POST",
          credentials: "include",
          mode: "cors",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email,
            password1,
            password2,
          }),
        }
      );

      if (res.status !== 200) {
        const data = await res.json();
        setErrorElements(
          <React.Fragment>
            {data.map((error) => (
              <li>{error}</li>
            ))}
            {!data.length ? <li>An error occurred.</li> : ""}
          </React.Fragment>
        );
        throw Error();
      } else {
        Router.push("/profile");
      }
    } catch (error) {
      setIsLoading(false);
    }

    setIsLoading(false);
  };

  const mainFormElements = (
    <React.Fragment>
      <div className="input-wrapper">
        <img src="/images/icons/mail.svg" alt="e-mail" />
        <input
          type="text"
          name="email"
          placeholder="Your email..."
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div className="input-wrapper">
        <img src="/images/icons/password.svg" alt="password" />
        <input
          type="password"
          name="password"
          placeholder="Your password..."
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
        />
      </div>
      <div className="input-wrapper">
        <img src="/images/icons/password.svg" alt="password confirmation" />
        <input
          type="password"
          name="confirm password"
          placeholder="Confirm password..."
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
        />
      </div>
    </React.Fragment>
  );

  const endElements = (
    <Link href="/login">
      <a>Already have an account?</a>
    </Link>
  );

  return (
    <React.Fragment>
      <Head>
        <title>E-Memories - Register</title>
      </Head>
      <TheForm
        title="Register"
        onSubmit={onSubmitRegister}
        mainFormElements={mainFormElements}
        image="/images/register/memory.jpg"
        endElements={endElements}
        isLoading={isLoading}
        errorElements={errorElements}
      />
    </React.Fragment>
  );
}

export default Register;
