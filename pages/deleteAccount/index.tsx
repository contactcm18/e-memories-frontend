import React, { useState } from "react";
import { useRouter } from "next/router";
import { GetServerSideProps } from "next";
import Link from "next/link";

import { isAuthenticated } from "lib/fetch";
import TheForm from "components/TheForm";

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const { authenticated } = await isAuthenticated(req, res);
  return {
    ...(!authenticated && {
      redirect: {
        destination: "/register",
        statusCode: 302,
      },
    }),
    props: {},
  };
};

function DeleteAccount() {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState("");

  const [errorElements, setErrorElements] = useState(
    <React.Fragment></React.Fragment>
  );

  const deleteAccount = async () => {
    if (isLoading) return;
    if (!email) {
      setErrorElements(<li>Please enter your email for confirmation.</li>);
      return;
    }
    setIsLoading(true);

    try {
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/deleteAccount`,
        {
          method: "DELETE",
          mode: "cors",
          credentials: "include",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ confirmationEmail: email }),
        }
      );

      if (res.status !== 200) {
        throw Error();
      } else {
        router.push("/register");
      }
    } catch (error) {
      setErrorElements(<li>This is not your email.</li>);
      setIsLoading(false);
    }
  };

  const mainFormElements = (
    <React.Fragment>
      <p>All your memories will be deleted!</p>
      <div className="input-wrapper">
        <img src="/images/icons/mail.svg" alt="Email" />
        <input
          type="text"
          name="your email"
          placeholder="Your email..."
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
    </React.Fragment>
  );

  const endElements = (
    <Link href="/memories">
      <a>Back to Memories</a>
    </Link>
  );

  return (
    <TheForm
      title="Delete Account"
      image="/images/forgot_password/security.jpg"
      onSubmit={deleteAccount}
      isLoading={isLoading}
      mainFormElements={mainFormElements}
      errorElements={errorElements}
      endElements={endElements}
    />
  );
}

export default DeleteAccount;
