import { useState, useEffect } from "react";
import Head from "next/head";

import FullLoad from "components/FullLoad";
import Quote from "components/Quote";
import Memory from "components/Memory";
import CreateMemory from "components/CreateMemory";

import styles from "./styles.module.scss";

function Memories() {
  const [memories, setMemories] = useState(null);
  const [quote, setQuote] = useState(null);
  const [memoryReceiveTimes, setMemoryReceiveTimes] = useState(null);
  const [myReceiveTime, setMyReceiveTime] = useState(null);

  useEffect(() => {
    try {
      fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/user/getMemories`, {
        credentials: "include",
      })
        .then((res) => res.json())
        .then((data) => {
          setMyReceiveTime(data.receiveTime);
          setMemories(data.memories);
        });
      fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/user/getQuote`)
        .then((res) => res.json())
        .then((data) => setQuote(data));
      fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/user/memoryReceiveTimes`)
        .then((res) => res.json())
        .then((data) => setMemoryReceiveTimes(data));
    } catch (error) {
      console.log(error);
    }
  }, []);

  if (memories == null || quote == null || memoryReceiveTimes == null)
    return <FullLoad />;

  const onDeleteMemory = (id) => {
    fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/user/deleteMemory`, {
      method: "DELETE",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ memoryId: id }),
    });
    setMemories((previousValue) =>
      previousValue.filter((memory) => memory.id !== id)
    );
  };

  const onCreateMemory = (text: string) => {
    fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/user/addMemory`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ memory: text }),
    })
      .then((res) => res.json())
      .then((data) => {
        setMemories((previousValue) => [data, ...previousValue]);
      });
  };

  const getMemoryReceiveTimes = () => {
    let receiveTimes: Array<{ key: string; value: string }> = [];
    for (const receiveTime in memoryReceiveTimes) {
      receiveTimes.push({
        key: receiveTime,
        value: memoryReceiveTimes[receiveTime],
      });
    }
    return receiveTimes;
  };

  const editReceiveMemoryTime = (event) => {
    fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/user/setReceiveTime`, {
      method: "POST",
      mode: "cors",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ receiveTime: event.target.value }),
    });
    setMyReceiveTime(event.target.value);
  };

  return (
    <main className={`container clear-top ${styles.memoriesPage}`}>
      <Head>
        <title>Your Memories - E-Memories</title>
      </Head>
      <div className="row">
        <div className="col-24 d-flex justify-content-center">
          <Quote quote={quote} />
        </div>
        <div className="col-1 col-sm-2 col-md-3 col-xl-6"></div>
        <div className="col-22 col-sm-9 col-md-8 col-xl-5">
          <h1>Your memories</h1>
          <ul className={styles.memoriesWrapper}>
            {memories.length ? (
              memories.map((memory) => (
                <Memory
                  memory={memory}
                  onDeleteMemory={onDeleteMemory}
                  key={memory.id}
                />
              ))
            ) : (
              <li>You don't have any memories!</li>
            )}
          </ul>
        </div>
        <div className="col-1 col-sm-2"></div>
        <div className="col-1 d-sm-none"></div>
        <div className="col-22 col-sm-9 col-md-8 col-xl-5">
          <h2>Create memory</h2>
          <CreateMemory onCreateMemory={onCreateMemory} />
        </div>
        <div className="col-24 d-flex flex-column justify-content-center align-items-center">
          <div className={styles.memoryTime}>
            <p>Receive memory every</p>
            <select
              name="receive memory every"
              onChange={editReceiveMemoryTime}
              value={myReceiveTime}
            >
              {getMemoryReceiveTimes().map((receiveTime) => (
                <option value={receiveTime.value} key={receiveTime.key}>
                  {receiveTime.value}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    </main>
  );
}

export default Memories;
