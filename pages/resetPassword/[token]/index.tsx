import React, { useState } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import Link from "next/link";

import ResetPassword from "components/ResetPassword";

function Token() {
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const defaultElements = <React.Fragment></React.Fragment>;
  const [successElements, setSuccessElements] = useState(defaultElements);
  const [errorElements, setErrorElements] = useState(defaultElements);

  const router = useRouter();

  const requestPasswordReset = async () => {
    try {
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/resetPassword/${router.query.token}`,
        {
          method: "PUT",
          mode: "cors",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            password1,
            password2,
          }),
        }
      );

      if (res.status !== 200) {
        const data = await res.json();
        setErrorElements(data.map((error) => <li>{error}</li>));
      } else {
        setErrorElements(<React.Fragment></React.Fragment>);
        setSuccessElements(
          <li>
            <Link href="/login">
              <a>Your password has been reset. Login here</a>
            </Link>
          </li>
        );
      }
    } catch (error) {
      setErrorElements(<li>An error has occurred.</li>);
    }
  };

  const mainFormElements = (
    <React.Fragment>
      <div className="input-wrapper">
        <img src="/images/icons/mail.svg" alt="email" />
        <input
          type="password"
          name="password"
          placeholder="Your new password..."
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
        />
      </div>
      <div className="input-wrapper">
        <img src="/images/icons/mail.svg" alt="email" />
        <input
          type="password"
          name="password confirmation"
          placeholder="Confirm password..."
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
        />
      </div>
    </React.Fragment>
  );

  return (
    <React.Fragment>
      <Head>
        <title>Enter Your New Password - E-Memories</title>
      </Head>
      <ResetPassword
        onResetPassword={requestPasswordReset}
        mainFormElements={mainFormElements}
        successElements={successElements}
        errorElements={errorElements}
      />
    </React.Fragment>
  );
}

export default Token;
