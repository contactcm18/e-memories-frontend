import React, { useState } from "react";
import Head from "next/head";

import ResetPassword from "components/ResetPassword";

function ForgotPassword() {
  const [email, setEmail] = useState("");
  const defaultElements = <React.Fragment></React.Fragment>;
  const [successElements, setSuccessElements] = useState(defaultElements);
  const [errorElements, setErrorElements] = useState(defaultElements);

  const requestPasswordReset = async () => {
    if (!email) {
      setErrorElements(<li>Please enter your email.</li>);
      return;
    }
    try {
      const res = await fetch(
        `${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/resetPassword`,
        {
          method: "PUT",
          mode: "cors",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email,
          }),
        }
      );

      if (res.status !== 200) {
        throw Error();
      } else {
        setErrorElements(<React.Fragment></React.Fragment>);
        setSuccessElements(
          <li>Email has been sent. Please check your inbox.</li>
        );
      }
    } catch (error) {
      setErrorElements(<li>Error in sending email.</li>);
    }
  };

  const mainFormElements = (
    <React.Fragment>
      <div className="input-wrapper">
        <img src="/images/icons/mail.svg" alt="email" />
        <input
          type="text"
          name="email"
          placeholder="Your email..."
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
    </React.Fragment>
  );

  return (
    <React.Fragment>
      <Head>
        <title>Reset Password - E-Memories</title>
      </Head>
      <ResetPassword
        onResetPassword={requestPasswordReset}
        mainFormElements={mainFormElements}
        successElements={successElements}
        errorElements={errorElements}
      />
    </React.Fragment>
  );
}

export default ForgotPassword;
