import Head from "next/head";
import { GetServerSideProps } from "next";

import Link from "next/link";
import Quote from "components/Quote";

import { isAuthenticated, getQuote, IQuote } from "lib/fetch";

import styles from "./styles.module.scss";

export const getServerSideProps: GetServerSideProps = async ({ req, res }) => {
  const { authenticated, email } = await isAuthenticated(req, res);
  if (!authenticated) {
    return {
      redirect: {
        destination: "/login",
        permament: false,
      },
      props: {},
    };
  }

  const quote: IQuote = await getQuote();

  return {
    props: {
      quote,
      user: {
        email,
      },
    },
  };
};

function Profile({ quote, user }: { quote: IQuote; user: any }) {
  return (
    <main className="clear-top">
      <Head>
        <title>Your Profile - E-Memories</title>
      </Head>
      <div className="container">
        <div className="row">
          <div className="col-24 d-flex justify-content-center">
            <Quote quote={quote} />
          </div>
          <div className="col-2 col-sm-3 col-md-5"></div>
          <div className={`col-20 col-sm-18 col-md-14 ${styles.profileText}`}>
            <h1>You are registered under</h1>
            <p>{user.email}</p>
            <Link href="/memories">
              <a className={styles.myMemories}>My memories</a>
            </Link>
            <Link href="/logout">
              <a>Logout</a>
            </Link>
            <Link href="/resetPassword">
              <a>Reset password</a>
            </Link>
            <Link href="/deleteAccount">
              <a>Delete Account</a>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
}

export default Profile;
