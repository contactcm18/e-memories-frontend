import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";

export default function Home() {
  return (
    <main className={styles.home}>
      <Head>
        <title>E-Memories - Your Memories By Mail</title>
      </Head>
      <div className={styles.backgroundOverlay}>
        <div className={styles.content}>
          <h1>e-memories</h1>
          <h2>Receive memories you want to remember by email</h2>
          <p>every</p>
          <div className={styles.timeFlip}>
            <ul className={styles.animatorWrapper}>
              <li>Day</li>
              <li>3 Days</li>
              <li>Week</li>
              <li>Month</li>
            </ul>
          </div>
          <Link href="/register">
            <a className="button">Get Started</a>
          </Link>
        </div>
      </div>
    </main>
  );
}
