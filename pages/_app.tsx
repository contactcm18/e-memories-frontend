import React, { useState, useEffect } from "react";

import MobileNav from "components/MobileNav";
import TopBar from "components/TopBar";

import "../styles/globals.scss";
import "../styles/grid.scss";

// import { wrapper } from "store";

function MyApp({ Component, pageProps }) {
  const [isLoggedIn, setIsLoggedIn] = useState(null);
  const [toggleSideNav, setToggleSideNav] = useState(false);

  useEffect(() => {
    try {
      fetch(`${process.env.NEXT_PUBLIC_BACKEND_HOST}/auth/isAuthenticated`, {
        method: "POST",
        credentials: "include",
      }).then(async (res) => {
        if (res.status === 200) {
          setIsLoggedIn(true);
        } else {
          setIsLoggedIn(false);
        }
      });
    } catch (error) {}
  });

  const onToggleSideNav = (value?: boolean) => {
    if (value !== null && typeof value === "boolean") setToggleSideNav(value);
    else setToggleSideNav((previousValue) => !previousValue);
  };

  return (
    <React.Fragment>
      <TopBar
        isLoggedIn={isLoggedIn}
        toggleSideNav={toggleSideNav}
        onToggleSideNav={onToggleSideNav}
      />
      <MobileNav
        isLoggedIn={isLoggedIn}
        toggleSideNav={toggleSideNav}
        onToggleSideNav={onToggleSideNav}
      />
      <Component {...pageProps} />
    </React.Fragment>
  );
}

// export default wrapper.withRedux(MyApp);
export default MyApp;
